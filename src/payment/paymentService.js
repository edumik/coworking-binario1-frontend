export const createSubscriber = async (data = {
    name: "",
    surname: "",
    email: "",
    start_date: "",
    start_hour: "",
    "num_of_hours": 0,
    note: ''
}) => {

    const fetchResult = await fetch(`${process.env.VUE_APP_HOST}/api/subscriber`, getPostObject(data));
    const result = await fetchResult.json();
    return result;
}

export const createPayment = async (data = {
    subscriber_id: 0
}) => {
    const fetchResult = await fetch(`${process.env.VUE_APP_HOST}/api/payments/payment`, getPostObject(data));
    const result = await fetchResult.json();
    return result;
}

export const executePayment = async (data = {
    payment_id: "",
    payer_id: ""
}) => {
    const fetchResult = await fetch(`${process.env.VUE_APP_HOST}/api/payments/execute`, getPostObject(data));
    const result = await fetchResult.json();
    return result;
}

export const getBookingsForDay = async (data = {
    day: ''
}) => {
    const fetchResult = await fetch(`${process.env.VUE_APP_HOST}/api/bookings?day=${data.day}`);
    const result = await fetchResult.json();
    return result;
}

export const getDefaultData = async () => {
    const fetchResult = await fetch(`${process.env.VUE_APP_HOST}/api/configurations`);
    const result = await fetchResult.json();
    return result;
}

function getPostObject(data) {
    return {
        method: 'POST',
        headers: {
            "Content-Type": "application/json",
        },
        body: JSON.stringify(data)
    }
}

export const acceptBooking = async(data) => {
    console.log(data);
    await fetch(`${process.env.VUE_APP_HOST}/api/payments/acceptBooking`, getPostObject(data));
    //const result = await fetchResult.json();
    //return result;
    return;
}