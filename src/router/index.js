import Vue from 'vue';
import Router from 'vue-router';
import Main from '@/components/Main';
import PaymentAccepted from '@/components/PaymentAccepted';
import PaymentRejected from '@/components/PaymentRejected';
import Dashboard from '@/components/Dashboard';
import Callback from '@/components/Callback';
import Logout from '@/components/Logout';
// .. other imports
import auth from "../auth/authService";

Vue.use(Router);

const router = new Router({
  base: process.env.NODE_ENV === 'production'
    ? '/coworkingApp-client-master/' //prod address
    : '/coworkingApp-client/', // dev address
  mode: 'history',
  routes: [
    {
      path: '/payment-accepted/',
      name: 'PaymentAccepted',
      component: PaymentAccepted,
      props: true
    },
    {
      path: '/payment-rejected/',
      name: 'PaymentRejected',
      component: PaymentRejected,
      props: true
    },
    {
      path: '/dashboard/',
      name: 'Dashboard',
      component: Dashboard,
      props: true,
      beforeEnter: (to, _, next) => {
        console.log('is authenticated?', auth.isAuthenticated() );
        if (auth.isAuthenticated()) {
          return next();
        }
        auth.login({ target: to.path });
      }
    },
    {
      path: '/callback',
      name: 'callback',
      component: Callback
    },
    {
      path: '/logout',
      name: 'logout',
      component: Logout
    },
    {
      path: '*',
      name: 'Main',
      component: Main,
      props: true
    },
  ]
});

export default router;
