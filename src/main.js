import Vue from 'vue'
import App from './App.vue';
import router from './router';

import BootstrapVue from 'bootstrap-vue';
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap-vue/dist/bootstrap-vue.css';
Vue.use(BootstrapVue);


import AuthPlugin from "./plugins/auth";
Vue.use(AuthPlugin);

Vue.config.productionTip = false

import { getDefaultData } from './payment/paymentService';

export let conf = null;
getDefaultData().then((configuration) => {

  conf = configuration;
  conf.opening_time = +conf.opening_time;
  conf.closing_time = +conf.closing_time;
  conf.hourly_cost = conf.hourly_cost.split(';').map(Number);
  conf.max_booking_for_subscriber = +conf.max_booking_for_subscriber;
  conf.whole_room_cost = +conf.whole_room_cost;


  new Vue({
    router,
    render: h => h(App),
  }).$mount('#app')

});
