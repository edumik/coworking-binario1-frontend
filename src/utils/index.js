export const leftpad = function leftpad (num) {
  const str = '' + num;
  const pad = '00';
  return pad.substring(0, pad.length - str.length) + str;
};

export const getDateAsYYYYMMDD = date => `${date.getFullYear()}-${leftpad(date.getMonth()+1)}-${leftpad(date.getDate())}`

export const getDateFromYYYMMDD = dateString => {
  const [yyyy, mm, dd] = dateString.split('-');
  return new Date(yyyy, mm, dd, 12, 0, 0, 0);
}

export const validateEmail = function validateEmail (email) {
  var re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(String(email).toLowerCase());
};

export const pagamentoAccettato = (form, pinCode) => {
  return `
  <h1>Pagamento Accettato</h1>
      <p>Il tuo pagamento è stato ricevuto.</p>
      <p>
        Il codice per connettersi al wifi è
        <strong>${pinCode}</strong>.
      </p>
      <p>
        Potrai usarlo per collegarti a internet una volta nella sala del coworking. Dal primo collegamento, avrai
        <strong>${form.hours} ore di tempo</strong>.
      </p>
      <p>Se il codice non verrà utilizzato entro 30 minuti dall'orario previsto di inizio, il codice sarà invalidato.</p>
      <p>
        Il codice pin sarà
        <strong>inviato anche via email</strong>. Ti auguriamo di trascorrere con profitto le ore prenotate.
      </p>
`;
};
