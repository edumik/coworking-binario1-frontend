// vue.config.js

const HtmlWebpackExternalsPlugin = require('html-webpack-externals-plugin')

module.exports = {
    publicPath: process.env.NODE_ENV === 'production'
      ? '/coworkingApp-client-master/' //prod address
      : '/coworkingApp-client/', // dev address
      filenameHashing: false,
      configureWebpack: {
        plugins: [
          new HtmlWebpackExternalsPlugin({
            externals: [
              {
                module: 'paypal',
                entry: 'https://www.paypalobjects.com/api/checkout.js',
                global: 'paypal'
              }
            ]
          })
        ]
      }
  }
